const tabla = document.querySelector('#lista-Datos tbody');

// Funcion para mostrar los datos del archivo "Datos.json"
function cargarDatos() {
    fetch('Datos.json')
        .then(respuesta => respuesta.json()) 
        .then(Datos => {
            Datos.forEach(Dato => {
                const row = document.createElement('tr');
                row.innerHTML += `
                    <td>${Dato.cedula}</td>
                    <td>${Dato.nombre}</td>
                    <td>${Dato.direccion}</td>
                    <td>${Dato.telefono}</td>
                    <td>${Dato.correo}</td>
                    <td>${Dato.curso}</td>
                    <td>${Dato.paralelo}</td>
                `;
                tabla.appendChild(row);
            });
        }) 
        .catch(error => console.log('Se ha presentado un error: ' + error.message))
}
cargarDatos();
